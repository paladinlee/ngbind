import { Component, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  encapsulation:ViewEncapsulation.None
})
export class AppComponent {
  sEles=['Server_A','Server_B'];
  title = 'ngBind';
  defRandom:number=0;
  defElement=[{type:'server',name:'ser01',content:'This is server 01'},
  {type:'blueprint',name:'ser02',content:'This is server 02'}];



  getRandom(min,max){
    return Math.floor(Math.random()*max)+min;
};

  ngOnInit(){
    this.defRandom=this.getRandom(1,10);
  }

  
  OnServerCreated(serverData:{serverName:string,serverContent:string}){
    this.defElement.push({type:'server',name:serverData.serverName,content:serverData.serverContent});
  }

  OnBlueprintCreated(serverData:{serverName:string,serverContent:string}){
    this.defElement.push({type:'blueprint',name:serverData.serverName,content:serverData.serverContent});
  }

  /*
  OnServerCreated(serverDtat){
    console.log('OnServerCreated');
    console.log(serverDtat);
  }*/

  OnChangeFirst(){
    this.defElement[0].name="Been Changed";
    
  }

  OnDestroyFirst(){
    this.defElement.splice(0,1);
  }
}
