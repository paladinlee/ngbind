import { Component, OnInit, Input, EventEmitter, Output, ViewEncapsulation, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-cockpit',
  templateUrl: './cockpit.component.html',
  styleUrls: ['./cockpit.component.css'],
  encapsulation:ViewEncapsulation.None
})
export class CockpitComponent implements OnInit {
  //newServerName='';
  newServerContent='';
  @ViewChild('serverNameInput',{static:true}) vServerName:ElementRef;

  @Output() ServerCreated=new EventEmitter<{serverName:string,serverContent:string}>();
  @Output() BlueprintCreated=new EventEmitter<{serverName:string,serverContent:string}>();

  constructor() { }

  ngOnInit() {
  }

  onAddServer(serverName:HTMLInputElement){    
    console.log(this.vServerName.nativeElement.value);
    this.ServerCreated.emit({serverName:this.vServerName.nativeElement.value,serverContent:this.newServerContent});
  }

  onAddBlueprint(serverName:HTMLInputElement){    
    this.BlueprintCreated.emit({serverName:this.vServerName.nativeElement.value,serverContent:this.newServerContent});
  }

}
