import { Component, OnInit, Input, OnChanges, SimpleChanges, DoCheck, AfterContentInit, AfterContentChecked, AfterViewInit, AfterViewChecked, OnDestroy, ContentChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-server-element',
  templateUrl: './server-element.component.html',
  styleUrls: ['./server-element.component.css']
})
export class ServerElementComponent implements OnInit,
OnChanges,
DoCheck,
AfterContentInit,
AfterContentChecked,
AfterViewInit,
AfterViewChecked,
OnDestroy 
{
  ngOnDestroy(): void {
    console.log('ngOnDestroy called!');       
  }
  ngAfterViewInit(): void {
    console.log('ngAfterViewInit called!');      
  }
  ngAfterViewChecked(): void {
    console.log('ngAfterViewChecked called!');      
  }
  ngAfterContentChecked(): void {    
    console.log('ngAfterContentChecked called!');  
  }
  ngAfterContentInit(): void {
    console.log('ngAfterContentInit called!');  
    console.log('AfterContentInit=>Text content of paragraph:'+this.paragraph.nativeElement.textContent);
  }

  ngDoCheck(): void {
    console.log('ngDoCheck called!');  
  }

  @Input() serverElements=[]
  @Input() server_random:number=0
  @Input('srvElement') element:{type:string, name:string, content:string};
  @Input() Ename='';
  @ContentChild('contentParagraph',{static:true}) paragraph:ElementRef;
  constructor() { 
    console.log('constructor called!');
  }

  ngOnChanges(changes:SimpleChanges) {
    console.log('ngOnChanges called!');    
    console.log(changes);    
  } 
 
  

  ngOnInit() {
    console.log('ngOnInit called!');    
    console.log('OnInit=>Text content of paragraph:'+this.paragraph.nativeElement.textContent);    
  }

}
